# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-03 00:50+0000\n"
"PO-Revision-Date: 2022-05-05 07:39+0200\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Lokalize 21.12.2\n"

#: kategitblameplugin.cpp:96
#, kde-format
msgctxt "git-blame information \"author: date \""
msgid " %1: %2 "
msgstr " %1: %2 "

#: kategitblameplugin.cpp:97
#, kde-format
msgctxt "git-blame information \"author: date: commit title \""
msgid " %1: %2: %3 "
msgstr " %1: %2: %3 "

#: kategitblameplugin.cpp:165
#, kde-format
msgid "Git Blame"
msgstr "Git Blame"

#: kategitblameplugin.cpp:168
#, kde-format
msgid "Show Git Blame Details"
msgstr "Podrobnosti Git Blame"

#: kategitblameplugin.cpp:171
#, kde-format
msgid "Toggle Git Blame Details"
msgstr "Preklopi podrobnosti Git Blame"

#: kategitblameplugin.cpp:317
#, kde-format
msgid "Git"
msgstr "Git"

#: kategitblameplugin.cpp:457
#, kde-format
msgid "Git blame, show commit failed."
msgstr "Git blame, prikaz potrditve oz. objave ni uspel."

#: kategitblameplugin.cpp:544
#, kde-format
msgid "Not Committed Yet"
msgstr "Še ni potrjeno"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "Orodja"

#~ msgid "Close"
#~ msgstr "Zapri"

#~ msgid "Commit"
#~ msgstr "Potrdi"

#~ msgid "&View"
#~ msgstr "Pogled"

#~ msgid "Git blame failed."
#~ msgstr "Git blame je spodletel."
