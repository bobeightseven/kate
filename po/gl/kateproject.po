# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Marce Villarino <mvillarino@kde-espana.es>, 2012, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2016.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-05 00:50+0000\n"
"PO-Revision-Date: 2019-10-26 13:59+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.08.2\n"

#: branchcheckoutdialog.cpp:29
#, kde-format
msgid "Select branch to checkout. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:36
#, kde-format
msgid "Create New Branch"
msgstr ""

#: branchcheckoutdialog.cpp:38
#, kde-format
msgid "Create New Branch From..."
msgstr ""

#: branchcheckoutdialog.cpp:53
#, kde-format
msgid "Branch %1 checked out"
msgstr ""

#: branchcheckoutdialog.cpp:56
#, kde-format
msgid "Failed to checkout to branch %1, Error: %2"
msgstr ""

#: branchcheckoutdialog.cpp:83 branchcheckoutdialog.cpp:95
#, kde-format
msgid "Enter new branch name. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:100
#, kde-format
msgid "Select branch to checkout from. Press 'Esc' to cancel."
msgstr ""

#: branchcheckoutdialog.cpp:121
#, kde-format
msgid "Checked out to new branch: %1"
msgstr ""

#: branchcheckoutdialog.cpp:123
#, kde-format
msgid "Failed to create new branch. Error \"%1\""
msgstr ""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Branch"
msgstr ""

#: branchdeletedialog.cpp:122
#, kde-format
msgid "Last Commit"
msgstr ""

#: branchdeletedialog.cpp:135 kateprojecttreeviewcontextmenu.cpp:78
#, kde-format
msgid "Delete"
msgstr ""

#: branchdeletedialog.cpp:140
#, kde-format
msgid "Are you sure you want to delete the selected branch?"
msgid_plural "Are you sure you want to delete the selected branches?"
msgstr[0] ""
msgstr[1] ""

#: branchesdialog.cpp:102
#, kde-format
msgid "Select Branch..."
msgstr ""

#: branchesdialog.cpp:131 gitwidget.cpp:486 kateprojectpluginview.cpp:65
#, fuzzy, kde-format
#| msgid "&Git"
msgid "Git"
msgstr "&Git"

#: comparebranchesview.cpp:143
#, kde-format
msgid "Back"
msgstr ""

#: gitcommitdialog.cpp:69 gitcommitdialog.cpp:116
#, kde-format
msgid "Commit Changes"
msgstr ""

#: gitcommitdialog.cpp:73 gitcommitdialog.cpp:115 gitwidget.cpp:245
#, kde-format
msgid "Commit"
msgstr ""

#: gitcommitdialog.cpp:74
#, kde-format
msgid "Cancel"
msgstr ""

#: gitcommitdialog.cpp:76
#, kde-format
msgid "Write commit message..."
msgstr ""

#: gitcommitdialog.cpp:83
#, kde-format
msgid "Extended commit description..."
msgstr ""

#: gitcommitdialog.cpp:107
#, kde-format
msgid "Sign off"
msgstr ""

#: gitcommitdialog.cpp:111 gitcommitdialog.cpp:120
#, kde-format
msgid "Amend"
msgstr ""

#: gitcommitdialog.cpp:112 gitwidget.cpp:939
#, kde-format
msgid "Amend Last Commit"
msgstr ""

#: gitcommitdialog.cpp:119
#, kde-format
msgid "Amending Commit"
msgstr ""

#: gitcommitdialog.cpp:203
#, kde-format
msgctxt "Number of characters"
msgid "%1 / 52"
msgstr ""

#: gitcommitdialog.cpp:207
#, kde-format
msgctxt "Number of characters"
msgid "<span style=\"color:%1;\">%2</span> / 52"
msgstr ""

#: gitstatusmodel.cpp:85
#, kde-format
msgid "Staged"
msgstr ""

#: gitstatusmodel.cpp:87
#, fuzzy, kde-format
#| msgid "<untracked>"
msgid "Untracked"
msgstr "<non seguido>"

#: gitstatusmodel.cpp:89
#, kde-format
msgid "Conflict"
msgstr ""

#: gitstatusmodel.cpp:91
#, kde-format
msgid "Modified"
msgstr ""

#: gitwidget.cpp:253
#, kde-format
msgid "Git Push"
msgstr ""

#: gitwidget.cpp:266
#, kde-format
msgid "Git Pull"
msgstr ""

#: gitwidget.cpp:279
#, kde-format
msgid "Cancel Operation"
msgstr ""

#: gitwidget.cpp:287
#, kde-format
msgid " canceled."
msgstr ""

#: gitwidget.cpp:308 kateprojectview.cpp:69
#, kde-format
msgid "Filter..."
msgstr "Filtrar…"

#: gitwidget.cpp:394
#, kde-format
msgid "Failed to find .git directory for '%1', things may not work correctly"
msgstr ""

#: gitwidget.cpp:560
#, kde-format
msgid " error: %1"
msgstr ""

#: gitwidget.cpp:566
#, kde-format
msgid "\"%1\" executed successfully: %2"
msgstr ""

#: gitwidget.cpp:586
#, kde-format
msgid "Failed to stage file. Error:"
msgstr ""

#: gitwidget.cpp:599
#, kde-format
msgid "Failed to unstage file. Error:"
msgstr ""

#: gitwidget.cpp:610
#, kde-format
msgid "Failed to discard changes. Error:"
msgstr ""

#: gitwidget.cpp:621
#, kde-format
msgid "Failed to remove. Error:"
msgstr ""

#: gitwidget.cpp:637
#, kde-format
msgid "Failed to open file at HEAD: %1"
msgstr ""

#: gitwidget.cpp:669
#, kde-format
msgid "Failed to get Diff of file: %1"
msgstr ""

#: gitwidget.cpp:729
#, kde-format
msgid "Failed to commit: %1"
msgstr ""

#: gitwidget.cpp:733
#, kde-format
msgid "Changes committed successfully."
msgstr ""

#: gitwidget.cpp:743
#, kde-format
msgid "Nothing to commit. Please stage your changes first."
msgstr ""

#: gitwidget.cpp:756
#, kde-format
msgid "Commit message cannot be empty."
msgstr ""

#: gitwidget.cpp:871
#, kde-format
msgid "No diff for %1...%2"
msgstr ""

#: gitwidget.cpp:877
#, kde-format
msgid "Failed to compare %1...%2"
msgstr ""

#: gitwidget.cpp:892
#, kde-format
msgid "Failed to get numstat when diffing %1...%2"
msgstr ""

#: gitwidget.cpp:931
#, kde-format
msgid "Refresh"
msgstr ""

#: gitwidget.cpp:947
#, kde-format
msgid "Checkout Branch"
msgstr ""

#: gitwidget.cpp:959
#, kde-format
msgid "Delete Branch"
msgstr ""

#: gitwidget.cpp:971
#, kde-format
msgid "Compare Branch with..."
msgstr ""

#: gitwidget.cpp:975
#, kde-format
msgid "Show Commit"
msgstr ""

#: gitwidget.cpp:975
#, kde-format
msgid "Commit hash"
msgstr ""

#: gitwidget.cpp:980
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Commit..."
msgstr "Abrir con"

#: gitwidget.cpp:983 gitwidget.cpp:1020
#, kde-format
msgid "Stash"
msgstr ""

#: gitwidget.cpp:993
#, kde-format
msgid "Diff - stash"
msgstr ""

#: gitwidget.cpp:1024
#, kde-format
msgid "Pop Last Stash"
msgstr ""

#: gitwidget.cpp:1028
#, kde-format
msgid "Pop Stash"
msgstr ""

#: gitwidget.cpp:1032
#, kde-format
msgid "Apply Last Stash"
msgstr ""

#: gitwidget.cpp:1034
#, kde-format
msgid "Stash (Keep Staged)"
msgstr ""

#: gitwidget.cpp:1038
#, kde-format
msgid "Stash (Include Untracked)"
msgstr ""

#: gitwidget.cpp:1042
#, kde-format
msgid "Apply Stash"
msgstr ""

#: gitwidget.cpp:1043
#, kde-format
msgid "Drop Stash"
msgstr ""

#: gitwidget.cpp:1044
#, kde-format
msgid "Show Stash Content"
msgstr ""

#: gitwidget.cpp:1084
#, kde-format
msgid "Stage All"
msgstr ""

#: gitwidget.cpp:1086
#, kde-format
msgid "Remove All"
msgstr ""

#: gitwidget.cpp:1086
#, kde-format
msgid "Discard All"
msgstr ""

#: gitwidget.cpp:1089
#, kde-format
msgid "Open .gitignore"
msgstr ""

#: gitwidget.cpp:1090 gitwidget.cpp:1145 gitwidget.cpp:1195
#: kateprojectconfigpage.cpp:91 kateprojectconfigpage.cpp:102
#, kde-format
msgid "Show Diff"
msgstr ""

#: gitwidget.cpp:1107
#, kde-format
msgid "Are you sure you want to remove these files?"
msgstr ""

#: gitwidget.cpp:1116
#, kde-format
msgid "Are you sure you want to discard all changes?"
msgstr ""

#: gitwidget.cpp:1144
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open File"
msgstr "Abrir con"

#: gitwidget.cpp:1146
#, kde-format
msgid "Show in External Git Diff Tool"
msgstr ""

#: gitwidget.cpp:1147
#, kde-format
msgid "Open at HEAD"
msgstr ""

#: gitwidget.cpp:1148
#, kde-format
msgid "Unstage File"
msgstr ""

#: gitwidget.cpp:1148
#, kde-format
msgid "Stage File"
msgstr ""

#: gitwidget.cpp:1149
#, kde-format
msgid "Remove"
msgstr ""

#: gitwidget.cpp:1149
#, kde-format
msgid "Discard"
msgstr ""

#: gitwidget.cpp:1166
#, kde-format
msgid "Are you sure you want to discard the changes in this file?"
msgstr ""

#: gitwidget.cpp:1179
#, kde-format
msgid "Are you sure you want to remove this file?"
msgstr ""

#: gitwidget.cpp:1194
#, kde-format
msgid "Unstage All"
msgstr ""

#: gitwidget.cpp:1261
#, kde-format
msgid "Unstage Selected Files"
msgstr ""

#: gitwidget.cpp:1261
#, kde-format
msgid "Stage Selected Files"
msgstr ""

#: gitwidget.cpp:1262
#, kde-format
msgid "Discard Selected Files"
msgstr ""

#: gitwidget.cpp:1266
#, kde-format
msgid "Remove Selected Files"
msgstr ""

#: gitwidget.cpp:1279
#, kde-format
msgid "Are you sure you want to discard the changes?"
msgstr ""

#: gitwidget.cpp:1288
#, kde-format
msgid "Are you sure you want to remove these untracked changes?"
msgstr ""

#: kateproject.cpp:218
#, kde-format
msgid "Malformed JSON file '%1': %2"
msgstr ""

#: kateproject.cpp:523
#, kde-format
msgid "<untracked>"
msgstr "<non seguido>"

#: kateprojectcompletion.cpp:43
#, kde-format
msgid "Project Completion"
msgstr "Completado do proxecto"

#: kateprojectconfigpage.cpp:25
#, kde-format
msgctxt "Groupbox title"
msgid "Autoload Repositories"
msgstr "Cargar automaticamente os repositorios"

#: kateprojectconfigpage.cpp:27
#, kde-format
msgid ""
"Project plugin is able to autoload repository working copies when there is "
"no .kateproject file defined yet."
msgstr ""
"O complemento de proxectos pode cargar automaticamente copias locais de "
"repositorios cando aínda non hai un ficheiro «.kateproject» definido."

#: kateprojectconfigpage.cpp:30
#, kde-format
msgid "&Git"
msgstr "&Git"

#: kateprojectconfigpage.cpp:33
#, kde-format
msgid "&Subversion"
msgstr "&Subversion"

#: kateprojectconfigpage.cpp:35
#, kde-format
msgid "&Mercurial"
msgstr "&Mercurial"

#: kateprojectconfigpage.cpp:37
#, kde-format
msgid "&Fossil"
msgstr ""

#: kateprojectconfigpage.cpp:46
#, kde-format
msgctxt "Groupbox title"
msgid "Session Behavior"
msgstr ""

#: kateprojectconfigpage.cpp:47
#, kde-format
msgid "Session settings for projects"
msgstr ""

#: kateprojectconfigpage.cpp:48
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Restore Open Projects"
msgstr "Proxecto actual"

#: kateprojectconfigpage.cpp:55
#, kde-format
msgctxt "Groupbox title"
msgid "Project Index"
msgstr "Índice do proxecto"

#: kateprojectconfigpage.cpp:56
#, kde-format
msgid "Project ctags index settings"
msgstr "Configuración do índice de ctags do proxecto"

#: kateprojectconfigpage.cpp:57 kateprojectinfoviewindex.cpp:206
#, kde-format
msgid "Enable indexing"
msgstr "Activar a indexación"

#: kateprojectconfigpage.cpp:60
#, kde-format
msgid "Directory for index files"
msgstr ""

#: kateprojectconfigpage.cpp:64
#, kde-format
msgid ""
"The system temporary directory is used if not specified, which may overflow "
"for very large repositories"
msgstr ""

#: kateprojectconfigpage.cpp:71
#, kde-format
msgctxt "Groupbox title"
msgid "Cross-Project Functionality"
msgstr "Funcionalidade entre proxectos"

#: kateprojectconfigpage.cpp:72
#, kde-format
msgid ""
"Project plugin is able to perform some operations across multiple projects"
msgstr ""
"O complemento de proxectos pode realizar algunhas operacións entre varios "
"proxectos"

#: kateprojectconfigpage.cpp:73
#, kde-format
msgid "Cross-Project Completion"
msgstr "Completado entre proxectos"

#: kateprojectconfigpage.cpp:75
#, kde-format
msgid "Cross-Project Goto Symbol"
msgstr "Símbolo de destino entre proxectos"

#: kateprojectconfigpage.cpp:83
#, fuzzy, kde-format
#| msgid "&Git"
msgctxt "Groupbox title"
msgid "Git"
msgstr "&Git"

#: kateprojectconfigpage.cpp:84
#, kde-format
msgid "Show number of changed lines in git status"
msgstr ""

#: kateprojectconfigpage.cpp:88
#, kde-format
msgid "Single click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:90 kateprojectconfigpage.cpp:101
#, kde-format
msgid "No Action"
msgstr ""

#: kateprojectconfigpage.cpp:92 kateprojectconfigpage.cpp:103
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open file"
msgstr "Abrir con"

#: kateprojectconfigpage.cpp:93 kateprojectconfigpage.cpp:104
#, kde-format
msgid "Stage / Unstage"
msgstr ""

#: kateprojectconfigpage.cpp:99
#, kde-format
msgid "Double click action in the git status view"
msgstr ""

#: kateprojectconfigpage.cpp:135 kateprojectpluginview.cpp:64
#, kde-format
msgid "Projects"
msgstr "Proxectos"

#: kateprojectconfigpage.cpp:140
#, kde-format
msgctxt "Groupbox title"
msgid "Projects Properties"
msgstr "Propiedades dos proxectos"

#: kateprojectinfoview.cpp:35
#, kde-format
msgid "Terminal (.kateproject)"
msgstr "Terminal (.kateproject)"

#: kateprojectinfoview.cpp:43
#, kde-format
msgid "Terminal (Base)"
msgstr "Terminal (Base)"

#: kateprojectinfoview.cpp:50
#, kde-format
msgid "Code Index"
msgstr "Índice do código"

#: kateprojectinfoview.cpp:55
#, kde-format
msgid "Code Analysis"
msgstr "Análise do código"

#: kateprojectinfoview.cpp:60
#, kde-format
msgid "Notes"
msgstr "Notas"

#: kateprojectinfoviewcodeanalysis.cpp:31
#, kde-format
msgid "Start Analysis..."
msgstr "Iniciar a análise…"

#: kateprojectinfoviewcodeanalysis.cpp:44 kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "File"
msgstr "Ficheiro"

#: kateprojectinfoviewcodeanalysis.cpp:44 kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Line"
msgstr "Liña"

#: kateprojectinfoviewcodeanalysis.cpp:44
#, kde-format
msgid "Severity"
msgstr "Gravidade"

#: kateprojectinfoviewcodeanalysis.cpp:44
#, kde-format
msgid "Message"
msgstr "Mensaxe"

#: kateprojectinfoviewcodeanalysis.cpp:111
#, kde-format
msgid ""
"%1<br/><br/>The tool will be run on all project files which match this list "
"of file extensions:<br/><br/><b>%2</b>"
msgstr ""
"%1<br/><br/>A ferramenta executarase en todos os ficheiros de proxecto que "
"coinciden con esta lista de extensións de ficheiros:<br/><br/><b>%2</b>"

#: kateprojectinfoviewcodeanalysis.cpp:250
#, kde-format
msgid "Analysis on %1 file finished."
msgid_plural "Analysis on %1 files finished."
msgstr[0] "Rematou a análise de %1 ficheiro."
msgstr[1] "Rematou a análise de %1 ficheiros."

#: kateprojectinfoviewcodeanalysis.cpp:262
#, kde-format
msgid "Analysis on %1 file failed with exit code %2."
msgid_plural "Analysis on %1 files failed with exit code %2."
msgstr[0] "A análise de %1 ficheiro fallou co código de saída %2."
msgstr[1] "A análise de %1 ficheiros fallou co código de saída %2."

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Name"
msgstr "Nome"

#: kateprojectinfoviewindex.cpp:33
#, kde-format
msgid "Kind"
msgstr "Tipo"

#: kateprojectinfoviewindex.cpp:34
#, kde-format
msgid "Search"
msgstr "Buscar"

#: kateprojectinfoviewindex.cpp:195
#, kde-format
msgid "The index could not be created. Please install 'ctags'."
msgstr "Non se puido crear o índice. Instale «ctags»."

#: kateprojectinfoviewindex.cpp:205
#, kde-format
msgid "Indexing is not enabled"
msgstr ""

#: kateprojectitem.cpp:166
#, kde-format
msgid "Error"
msgstr ""

#: kateprojectitem.cpp:166
#, kde-format
msgid "File name already exists"
msgstr ""

#: kateprojectplugin.cpp:223
#, kde-format
msgid "Confirm project closing: %1"
msgstr ""

#: kateprojectplugin.cpp:224
#, kde-format
msgid "Do you want to close the project %1 and the related %2 open documents?"
msgstr ""

#: kateprojectplugin.cpp:576
#, kde-format
msgid "Full path to current project excluding the file name."
msgstr "A ruta completa do proxecto actual excluíndo o nome de ficheiro."

#: kateprojectplugin.cpp:593
#, kde-format
msgid ""
"Full path to current project excluding the file name, with native path "
"separator (backslash on Windows)."
msgstr ""
"Ruta completa do proxecto actual excluíndo o nome de ficheiro, co separador "
"de rutas nativo (barra invertida en Windows)."

#: kateprojectplugin.cpp:714 kateprojectpluginview.cpp:70
#: kateprojectpluginview.cpp:213 kateprojectviewtree.cpp:136
#: kateprojectviewtree.cpp:161
#, kde-format
msgid "Project"
msgstr "Proxecto"

#: kateprojectpluginview.cpp:54
#, fuzzy, kde-format
#| msgid "Kate Project Manager"
msgid "Project Manager"
msgstr "Xestor de proxectos de Kate"

#: kateprojectpluginview.cpp:80
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Reload project"
msgstr "Proxecto actual"

#: kateprojectpluginview.cpp:97
#, kde-format
msgid "Refresh git status"
msgstr ""

#: kateprojectpluginview.cpp:175
#, fuzzy, kde-format
#| msgid "Open With"
msgid "Open Folder..."
msgstr "Abrir con"

#: kateprojectpluginview.cpp:180
#, fuzzy, kde-format
#| msgid "Projects"
msgid "Project TODOs"
msgstr "Proxectos"

#: kateprojectpluginview.cpp:184
#, kde-format
msgid "Activate Previous Project"
msgstr ""

#: kateprojectpluginview.cpp:189
#, kde-format
msgid "Activate Next Project"
msgstr ""

#: kateprojectpluginview.cpp:194
#, kde-format
msgid "Lookup"
msgstr "Consulta"

#: kateprojectpluginview.cpp:198
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Project"
msgstr "Proxecto actual"

#: kateprojectpluginview.cpp:202
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close All Projects"
msgstr "Proxecto actual"

#: kateprojectpluginview.cpp:207
#, fuzzy, kde-format
#| msgid "Current Project"
msgid "Close Orphaned Projects"
msgstr "Proxecto actual"

#: kateprojectpluginview.cpp:216 kateprojectpluginview.cpp:791
#, kde-format
msgid "Lookup: %1"
msgstr "Buscar: %1"

#: kateprojectpluginview.cpp:217 kateprojectpluginview.cpp:792
#, kde-format
msgid "Goto: %1"
msgstr "Destino: %1"

#: kateprojectpluginview.cpp:296
#, kde-format
msgid "Projects Index"
msgstr "Índice de proxectos"

#: kateprojectpluginview.cpp:833
#, kde-format
msgid "Choose a directory"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:42
#, kde-format
msgid "Enter name:"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:43
#, kde-format
msgid "Add"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:64
#, fuzzy, kde-format
#| msgid "Follow Location"
msgid "Copy Location"
msgstr "Seguir o lugar"

#: kateprojecttreeviewcontextmenu.cpp:69
#, fuzzy, kde-format
#| msgid "File"
msgid "Add File"
msgstr "Ficheiro"

#: kateprojecttreeviewcontextmenu.cpp:70
#, kde-format
msgid "Add Folder"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:77
#, kde-format
msgid "&Rename"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:84
#, kde-format
msgid "Properties"
msgstr "Propiedades"

#: kateprojecttreeviewcontextmenu.cpp:88
#, kde-format
msgid "Open With"
msgstr "Abrir con"

#: kateprojecttreeviewcontextmenu.cpp:96
#, kde-format
msgid "Open Internal Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:105
#, kde-format
msgid "Open External Terminal Here"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:110
#, kde-format
msgid "&Open Containing Folder"
msgstr "Abrir o cartafol c&ontedor…"

#: kateprojecttreeviewcontextmenu.cpp:120
#, kde-format
msgid "Show Git History"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:130
#, kde-format
msgid "Delete File"
msgstr ""

#: kateprojecttreeviewcontextmenu.cpp:131
#, kde-format
msgid "Do you want to delete the file '%1'?"
msgstr ""

#: kateprojectview.cpp:60
#, kde-format
msgid "Checkout Git Branch"
msgstr ""

#: kateprojectview.cpp:83
#, kde-format
msgid "Checkout branch"
msgstr ""

#: kateprojectviewtree.cpp:138
#, kde-format
msgid "Failed to create file: %1, Error: %2"
msgstr ""

#: kateprojectviewtree.cpp:163
#, kde-format
msgid "Failed to create dir: %1"
msgstr ""

#: stashdialog.cpp:37
#, kde-format
msgid "Stash message (optional). Enter to confirm, Esc to leave."
msgstr ""

#: stashdialog.cpp:44
#, kde-format
msgid "Type to filter, Enter to pop stash, Esc to leave."
msgstr ""

#: stashdialog.cpp:143
#, kde-format
msgid "Failed to stash changes %1"
msgstr ""

#: stashdialog.cpp:145
#, kde-format
msgid "Changes stashed successfully."
msgstr ""

#: stashdialog.cpp:164
#, kde-format
msgid "Failed to get stash list. Error: "
msgstr ""

#: stashdialog.cpp:180
#, kde-format
msgid "Failed to apply stash. Error: "
msgstr ""

#: stashdialog.cpp:182
#, kde-format
msgid "Failed to drop stash. Error: "
msgstr ""

#: stashdialog.cpp:184
#, kde-format
msgid "Failed to pop stash. Error: "
msgstr ""

#: stashdialog.cpp:188
#, kde-format
msgid "Stash applied successfully."
msgstr ""

#: stashdialog.cpp:190
#, kde-format
msgid "Stash dropped successfully."
msgstr ""

#: stashdialog.cpp:192
#, kde-format
msgid "Stash popped successfully."
msgstr ""

#: stashdialog.cpp:221
#, kde-format
msgid "Show stash failed. Error: "
msgstr ""

#. i18n: ectx: Menu (projects)
#: ui.rc:9
#, kde-format
msgid "&Projects"
msgstr "&Proxectos"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino, Xosé Calvo"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@kde-espana.es, xosecalvo@gmail.com"

#~ msgid "Copy File Path"
#~ msgstr "Copiar a ruta do ficheiro"

#, fuzzy
#~| msgid "Open With"
#~ msgid "Type to filter..."
#~ msgstr "Abrir con"

#~ msgid "Git:"
#~ msgstr "Git:"

#, fuzzy
#~| msgid "<untracked>"
#~ msgid "Untracked (%1)"
#~ msgstr "<non seguido>"

#~ msgid ""
#~ "The index is not enabled. Please add '\"index\": true' to your ."
#~ "kateproject file."
#~ msgstr ""
#~ "O índice non está activado. Engada «'index': true» ao seu ficheiro ."
#~ "kateproject."

#~ msgid "Analysis failed!"
#~ msgstr "A análise fallou!"

#~ msgid "Please install 'cppcheck'."
#~ msgstr "Instale «cppcheck»."

#~ msgid "Git Tools"
#~ msgstr "Ferramentas do Git"

#~ msgid "Launch gitk"
#~ msgstr "Iniciar o gitk"

#~ msgid "Launch qgit"
#~ msgstr "Iniciar o qgit"

#~ msgid "Launch git-cola"
#~ msgstr "Iniciar o git-cola"

#~ msgid "Hello World"
#~ msgstr "Olá mundo"

#~ msgid "Example kate plugin"
#~ msgstr "Complemento de exemplo para Kate"
