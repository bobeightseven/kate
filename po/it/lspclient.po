# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
# Paolo Zamponi <zapaolo@email.it>, 2019, 2020, 2021, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-24 00:49+0000\n"
"PO-Revision-Date: 2022-11-13 17:15+0100\n"
"Last-Translator: Paolo Zamponi <zapaolo@email.it>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:250
#, kde-format
msgid "Filter..."
msgstr "Filtro..."

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:99 lspclientconfigpage.cpp:104
#: lspclientpluginview.cpp:600 lspclientpluginview.cpp:754 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "Client LSP"

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "No JSON data to validate."
msgstr "Nessun dato JSON da convalidare."

#: lspclientconfigpage.cpp:233
#, kde-format
msgid "JSON data is valid."
msgstr "Il dato JSON è valido."

#: lspclientconfigpage.cpp:235
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr "Il dato JSON non è valido: nessun oggetto JSON"

#: lspclientconfigpage.cpp:238
#, kde-format
msgid "JSON data is invalid: %1"
msgstr "Il dato JSON non è valido: %1"

#: lspclientconfigpage.cpp:286
#, kde-format
msgid "Delete selected entries"
msgstr "Elimina le voci selezionate"

#: lspclientconfigpage.cpp:291
#, kde-format
msgid "Delete all entries"
msgstr "Elimina tutte le voci"

#: lspclientplugin.cpp:231
#, kde-format
msgid "LSP server start requested"
msgstr "È richiesto l'avvio del server LSP"

#: lspclientplugin.cpp:234
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""
"Vuoi avviare il server LSP?<br><br>La riga di comando completa è:<br><br><b>"
"%1</b><br><br>La scelta può essere modificata dalla pagina di configurazione "
"dell'estensione."

#: lspclientplugin.cpp:247
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""
"L'utente ha bloccato permanentemente l'avvio di: «%1».\n"
"Usa la pagina di configurazione dell'estensione per annullare questo blocco."

#: lspclientpluginview.cpp:612
#, kde-format
msgid "Go to Definition"
msgstr "Vai alla definizione"

#: lspclientpluginview.cpp:614
#, kde-format
msgid "Go to Declaration"
msgstr "Vai alla dichiarazione"

#: lspclientpluginview.cpp:616
#, kde-format
msgid "Go to Type Definition"
msgstr "Vai alla definizione del tipo"

#: lspclientpluginview.cpp:618
#, kde-format
msgid "Find References"
msgstr "Trova riferimenti"

#: lspclientpluginview.cpp:621
#, kde-format
msgid "Find Implementations"
msgstr "Trova implementazioni"

#: lspclientpluginview.cpp:623
#, kde-format
msgid "Highlight"
msgstr "Evidenzia"

#: lspclientpluginview.cpp:625
#, kde-format
msgid "Symbol Info"
msgstr "Informazioni sul simbolo"

#: lspclientpluginview.cpp:627
#, kde-format
msgid "Search and Go to Symbol"
msgstr "Cerca e vai al simbolo"

#: lspclientpluginview.cpp:632
#, kde-format
msgid "Format"
msgstr "Formatta"

#: lspclientpluginview.cpp:635
#, kde-format
msgid "Rename"
msgstr "Rinomina"

#: lspclientpluginview.cpp:639
#, kde-format
msgid "Expand Selection"
msgstr "Espandi la selezione"

#: lspclientpluginview.cpp:642
#, kde-format
msgid "Shrink Selection"
msgstr "Restringi la selezione"

#: lspclientpluginview.cpp:646
#, kde-format
msgid "Switch Source Header"
msgstr "Scambia sorgente e intestazione"

#: lspclientpluginview.cpp:649
#, kde-format
msgid "Expand Macro"
msgstr "Espandi macro"

#: lspclientpluginview.cpp:651
#, kde-format
msgid "Quick Fix"
msgstr "Soluzione rapida"

#: lspclientpluginview.cpp:654
#, kde-format
msgid "Code Action"
msgstr "Azione sul codice"

#: lspclientpluginview.cpp:669
#, kde-format
msgid "Show selected completion documentation"
msgstr "Mostra la documentazione di completamento selezionata"

#: lspclientpluginview.cpp:672
#, kde-format
msgid "Enable signature help with auto completion"
msgstr "Abilita l'aiuto sulle firme con l'auto-completamento"

#: lspclientpluginview.cpp:675
#, kde-format
msgid "Include declaration in references"
msgstr "Includi la dichiarazione nei riferimenti"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:678 lspconfigwidget.ui:68
#, kde-format
msgid "Add parentheses upon function completion"
msgstr "Aggiungi le parentesi al completamento della funzione"

#: lspclientpluginview.cpp:681
#, kde-format
msgid "Show hover information"
msgstr "Mostra la finestrella informativa"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:684 lspconfigwidget.ui:234
#, kde-format
msgid "Format on typing"
msgstr "Formattazione durante la digitazione"

#: lspclientpluginview.cpp:687
#, kde-format
msgid "Incremental document synchronization"
msgstr "Sincronizzazione incrementale del documento"

#: lspclientpluginview.cpp:690
#, kde-format
msgid "Highlight goto location"
msgstr "Evidenzia posizione di arrivo"

#: lspclientpluginview.cpp:699
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:703
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr "Mostra le notifiche di diagnostica"

#: lspclientpluginview.cpp:706
#, kde-format
msgid "Show Diagnostics Highlights"
msgstr "Mostra i punti salienti della diagnostica"

#: lspclientpluginview.cpp:709
#, kde-format
msgid "Show Diagnostics Marks"
msgstr "Mostra i segni di diagnostica"

#: lspclientpluginview.cpp:712
#, kde-format
msgid "Show Diagnostics on Hover"
msgstr "Mostra diagnostica al passaggio del mouse"

#: lspclientpluginview.cpp:715
#, kde-format
msgid "Switch to Diagnostics Tab"
msgstr "Passa alla scheda di diagnostica"

#: lspclientpluginview.cpp:719
#, kde-format
msgid "Show Messages"
msgstr "Mostra messaggi"

#: lspclientpluginview.cpp:724
#, kde-format
msgid "Server Memory Usage"
msgstr "Utilizzo memoria nel server"

#: lspclientpluginview.cpp:728
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr "Chiudi tutte le schede di riferimento dinamiche"

#: lspclientpluginview.cpp:730
#, kde-format
msgid "Restart LSP Server"
msgstr "Riavvia server LSP"

#: lspclientpluginview.cpp:732
#, kde-format
msgid "Restart All LSP Servers"
msgstr "Riavvia tutti i server LSP"

#: lspclientpluginview.cpp:743
#, kde-format
msgid "Go To"
msgstr "Vai a"

#: lspclientpluginview.cpp:771
#, kde-format
msgid "More options"
msgstr "Più opzioni"

#: lspclientpluginview.cpp:804
#, kde-format
msgid "LSP"
msgstr "LSP"

#: lspclientpluginview.cpp:1000 lspclientpluginview.cpp:2561
#: lspclientsymbolview.cpp:285
#, kde-format
msgid "Expand All"
msgstr "Espandi tutto"

#: lspclientpluginview.cpp:1001 lspclientpluginview.cpp:2562
#: lspclientsymbolview.cpp:286
#, kde-format
msgid "Collapse All"
msgstr "Contrai tutto"

#: lspclientpluginview.cpp:1023
#, kde-format
msgctxt "@title:tab"
msgid "Diagnostics"
msgstr "Diagnostica"

#: lspclientpluginview.cpp:1287
#, kde-format
msgid "RangeHighLight"
msgstr "RangeHighLight"

#: lspclientpluginview.cpp:1293
#, kde-format
msgid "Error"
msgstr "Errore"

#: lspclientpluginview.cpp:1297
#, kde-format
msgid "Warning"
msgstr "Avviso"

#: lspclientpluginview.cpp:1301
#, kde-format
msgid "Information"
msgstr "Informazioni"

#: lspclientpluginview.cpp:1485
#, kde-format
msgctxt "@info"
msgid ""
"Error in regular expression: %1\n"
"offset %2: %3"
msgstr ""
"Errore nell'espressione regolare: %1\n"
"scostamento di %2: %3"

#: lspclientpluginview.cpp:1812
#, kde-format
msgid "Line: %1: "
msgstr "Riga: %1: "

#: lspclientpluginview.cpp:1959 lspclientpluginview.cpp:2311
#: lspclientpluginview.cpp:2432
#, kde-format
msgid "No results"
msgstr "Nessun risultato"

#: lspclientpluginview.cpp:2018
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "Definizione: %1"

#: lspclientpluginview.cpp:2024
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "Dichiarazione: %1"

#: lspclientpluginview.cpp:2030
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "Definizione del tipo: %1"

#: lspclientpluginview.cpp:2036
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "Riferimenti: %1"

#: lspclientpluginview.cpp:2048
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "Implementazione: %1"

#: lspclientpluginview.cpp:2061
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "Evidenzia: %1"

#: lspclientpluginview.cpp:2085 lspclientpluginview.cpp:2097
#: lspclientpluginview.cpp:2110
#, kde-format
msgid "No Actions"
msgstr "Nessuna azione"

#: lspclientpluginview.cpp:2101
#, kde-format
msgid "Loading..."
msgstr "Caricamento..."

#: lspclientpluginview.cpp:2193
#, kde-format
msgid "No edits"
msgstr "Nessuna modifica"

#: lspclientpluginview.cpp:2269
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "Rinomina"

#: lspclientpluginview.cpp:2270
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr ""
"Nuovo nome (attenzione: non tutti i riferimenti possono essere sostituiti)"

#: lspclientpluginview.cpp:2317
#, kde-format
msgid "Not enough results"
msgstr "Non ci sono abbastanza risultati"

#: lspclientpluginview.cpp:2387
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr "Intestazione o sorgente corrispondente non trovato"

#: lspclientpluginview.cpp:2571
#, kde-format
msgid "Copy to Clipboard"
msgstr "Copia negli appunti"

#: lspclientpluginview.cpp:2594
#, kde-format
msgid "Remove Global Suppression"
msgstr "Rimuovi soppressione globale"

#: lspclientpluginview.cpp:2596
#, kde-format
msgid "Add Global Suppression"
msgstr "Aggiungi soppressione globale"

#: lspclientpluginview.cpp:2600
#, kde-format
msgid "Remove Local Suppression"
msgstr "Rimuovi soppressione locale"

#: lspclientpluginview.cpp:2602
#, kde-format
msgid "Add Local Suppression"
msgstr "Aggiungi soppressione locale"

#: lspclientpluginview.cpp:2614
#, kde-format
msgid "Disable Suppression"
msgstr "Disabilita soppressione"

#: lspclientpluginview.cpp:2616
#, kde-format
msgid "Enable Suppression"
msgstr "Abilita soppressione"

#: lspclientpluginview.cpp:2762
#, kde-format
msgctxt "@info"
msgid "%1 [suppressed: %2]"
msgstr "%1 [soppresso: %2]"

#: lspclientpluginview.cpp:2877 lspclientpluginview.cpp:2914
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr "Server LSP"

#: lspclientpluginview.cpp:2937
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "Client LSP"

#: lspclientservermanager.cpp:582
#, kde-format
msgid "Restarting"
msgstr "Riavvio"

#: lspclientservermanager.cpp:582
#, kde-format
msgid "NOT Restarting"
msgstr "NON riavvio"

#: lspclientservermanager.cpp:583
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr "Server terminato inaspettatamente ... %1 [%2] [pagina principale: %3] "

#: lspclientservermanager.cpp:779
#, kde-format
msgid "Failed to find server binary: %1"
msgstr "Impossibile trovare il binario del server: %1"

#: lspclientservermanager.cpp:782 lspclientservermanager.cpp:817
#, kde-format
msgid "Please check your PATH for the binary"
msgstr "Controlla il PATH del binario"

#: lspclientservermanager.cpp:783 lspclientservermanager.cpp:818
#, kde-format
msgid "See also %1 for installation or details"
msgstr "Per i dettagli sull'installazione, vedi anche %1"

#: lspclientservermanager.cpp:814
#, kde-format
msgid "Failed to start server: %1"
msgstr "Impossibile avviare il server: %1"

#: lspclientservermanager.cpp:822
#, kde-format
msgid "Started server %2: %1"
msgstr "Server avviato %2: %1"

#: lspclientservermanager.cpp:856
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""
"Impossibile elaborare la configurazione del server «%1»: nessun oggetto JSON"

#: lspclientservermanager.cpp:859
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr "Impossibile elaborare la configurazione del server «%1»: %2"

#: lspclientservermanager.cpp:863
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr "Impossibile leggere la configurazione del server: %1"

#: lspclientsymbolview.cpp:238
#, kde-format
msgid "Symbol Outline"
msgstr "Schema dei simboli"

#: lspclientsymbolview.cpp:276
#, kde-format
msgid "Tree Mode"
msgstr "Modalità ad albero"

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Automatically Expand Tree"
msgstr "Espandi automaticamente l'albero"

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Sort Alphabetically"
msgstr "Ordina alfabeticamente"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Show Details"
msgstr "Mostra i dettagli"

#: lspclientsymbolview.cpp:437
#, kde-format
msgid "Symbols"
msgstr "Simboli"

#: lspclientsymbolview.cpp:568
#, kde-format
msgid "No LSP server for this document."
msgstr "Nessun server LSP per questo documento."

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr "Impostazioni del client"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:47
#, kde-format
msgid "Completions:"
msgstr "Completamenti:"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:54
#, kde-format
msgid "Show inline docs for selected completion"
msgstr "Mostra la documentazione in linea per il completamento selezionato"

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""
"Mostra le firma della funzione quando viene digitata una chiamata a funzione"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Diagnostics:"
msgstr "Diagnostica:"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show program diagnostics"
msgstr "Mostra diagnostica del programma"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsHighlight)
#: lspconfigwidget.ui:97
#, kde-format
msgid "Highlight lines with diagnostics"
msgstr "Evidenzia le righe con diagnostica"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsMark)
#: lspconfigwidget.ui:104
#, kde-format
msgid "Show markers in the margins for lines with diagnostics"
msgstr "Mostra i marcatori nei margini per le righe con diagnostica"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsHover)
#: lspconfigwidget.ui:111
#, kde-format
msgid "Show diagnostics on hover"
msgstr "Mostra diagnostica al passaggio del mouse"

#. i18n: ectx: property (toolTip), widget (QSpinBox, spinDiagnosticsSize)
#: lspconfigwidget.ui:118
#, kde-format
msgid "max diagnostics tooltip size"
msgstr "dimensione massima del suggerimento di diagnostica"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:133
#, kde-format
msgid "Navigation:"
msgstr "Navigazione:"

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:140
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr "Conta le dichiarazioni quando si cercano i riferimenti a un simbolo"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:147
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr "Mostra le informazioni sul simbolo sotto il puntatore"

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:154
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr "Evidenzia la riga di destinazione quando si salta ad essa"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:161
#, kde-format
msgid "Server:"
msgstr "Server:"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:168
#, kde-format
msgid "Show notifications from the LSP server"
msgstr "Mostra le notifiche del server LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:175
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr "Sincronizza in modo incrementale i documenti col server LSP"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:182
#, kde-format
msgid "Display additional details for symbols"
msgstr "Visualizza dettagli aggiuntivi per i simboli"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:189
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr "Presenta i simboli in modo gerarchico invece che in una lista piatta"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:204
#, kde-format
msgid "Automatically expand tree"
msgstr "Espandi automaticamente l'albero"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:213
#, kde-format
msgid "Sort symbols alphabetically"
msgstr "Ordina i simboli alfabeticamente"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:220
#, kde-format
msgid "Document outline:"
msgstr "Schema del documento:"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Enable semantic highlighting"
msgstr "Abilita l'evidenziazione semantica"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:241
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""
"Aggiungi le importazioni automaticamente se necessario al completamento"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:248
#, kde-format
msgid "Format on save"
msgstr "Formattazione al salvataggio"

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:255
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:278
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr "Server permessi &e server bloccati"

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:288
#, kde-format
msgid "User Server Settings"
msgstr "Impostazioni del server utente"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:296
#, kde-format
msgid "Settings File:"
msgstr "File impostazioni:"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:323
#, kde-format
msgid "Default Server Settings"
msgstr "Impostazioni predefinite del server"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr "Più opzioni"

#~ msgid "Type to filter through symbols in your project..."
#~ msgstr "Digita per filtrare i simboli nel tuo progetto..."

#~ msgid "LSP Client Symbol Outline"
#~ msgstr "Struttura dei simboli del client LSP"

#~ msgid "General Options"
#~ msgstr "Opzioni generali"

#~ msgid "Add highlights"
#~ msgstr "Aggiungi evidenziazione"

#~ msgid "Add markers"
#~ msgstr "Aggiungi marcatori"

#~ msgid "On hover"
#~ msgstr "Al passaggio del mouse"

#~ msgid "Tree mode outline"
#~ msgstr "Struttura in modalità ad albero"

#~ msgid "Automatically expand nodes in tree mode"
#~ msgstr "Espandi automaticamente i nodi nella modalità ad albero"

#~ msgid "Switch to messages tab upon message level"
#~ msgstr "Passa alla scheda dei messaggi a livello di messaggio"

#~ msgctxt "@info"
#~ msgid "Never"
#~ msgstr "Mai"

#~ msgctxt "@info"
#~ msgid "Error"
#~ msgstr "Errore"

#~ msgctxt "@info"
#~ msgid "Warning"
#~ msgstr "Attenzione"

#~ msgctxt "@info"
#~ msgid "Information"
#~ msgstr "Informazioni"

#~ msgctxt "@info"
#~ msgid "Log"
#~ msgstr "Registro"

#~ msgid "Switch to messages tab"
#~ msgstr "Passa alla scheda dei messaggi"

#~ msgctxt "@title:tab"
#~ msgid "Messages"
#~ msgstr "Messaggi"

#~ msgctxt "@info"
#~ msgid "Unknown"
#~ msgstr "Sconosciuto"

#~ msgid "Switch to messages tab upon level"
#~ msgstr "Passa alla scheda dei messaggi a livello"

#~ msgid "Never"
#~ msgstr "Mai"

#~ msgid "Log"
#~ msgstr "Registro"

#~ msgid "Hover"
#~ msgstr "Finestrella informativa"

#~ msgctxt "@info"
#~ msgid "<b>LSP Client:</b> %1"
#~ msgstr "<b>Client LSP:</b> %1"

#~ msgid "Server Configuration"
#~ msgstr "Configurazione del server"

#, fuzzy
#~| msgid "Format"
#~ msgid "Form"
#~ msgstr "Formatta"
