# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Nick Shaforostoff <shaforostoff@kde.ru>, 2009, 2011.
# Alexander Potashev <aspotashev@gmail.com>, 2010, 2011, 2015.
# Alexander Lakhin <exclusion@gmail.com>, 2014.
# Alexander Yavorsky <kekcuha@gmail.com>, 2019, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-27 00:48+0000\n"
"PO-Revision-Date: 2021-12-07 21:22+0300\n"
"Last-Translator: Alexander Yavorsky <kekcuha@gmail.com>\n"
"Language-Team: Russian <kde-russian@lists.kde.ru>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.08.3\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: kateconsole.cpp:54
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr "Вам запрещён доступ к терминалу"

#: kateconsole.cpp:102 kateconsole.cpp:132 kateconsole.cpp:605
#, kde-format
msgid "Terminal"
msgstr "Терминал"

#: kateconsole.cpp:141
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Перенаправить в терминал"

#: kateconsole.cpp:145
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "Синхронизировать терминал с текущим документом"

#: kateconsole.cpp:149
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Запустить выполнение текущего документа"

#: kateconsole.cpp:154 kateconsole.cpp:458
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Показать панель &терминала"

#: kateconsole.cpp:160
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Активировать терминал"

#: kateconsole.cpp:334
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Перенаправить текст в консоль? Для этого будут запущены соответствующие "
"команды (от вашего имени)."

#: kateconsole.cpp:335
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Перенаправить в терминал?"

#: kateconsole.cpp:336
#, kde-format
msgid "Pipe to Terminal"
msgstr "Перенаправить в терминал"

#: kateconsole.cpp:364
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "К сожалению, перейти в «%1» не удалось"

#: kateconsole.cpp:399
#, kde-format
msgid "Not a local file: '%1'"
msgstr "«%1» не является локальным файлом."

#: kateconsole.cpp:432
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Запустить выполнение документа?\n"
"Это приведёт к выполнению в терминале \n"
"под вашим именем пользователя\n"
"следующих команд:\n"
"«%1»"

#: kateconsole.cpp:439
#, kde-format
msgid "Run in Terminal?"
msgstr "Запустить в терминале?"

#: kateconsole.cpp:440
#, kde-format
msgid "Run"
msgstr "Запустить"

#: kateconsole.cpp:455
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "&Скрыть панель терминала"

#: kateconsole.cpp:466
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Деактивировать панель терминала"

#: kateconsole.cpp:467 kateconsole.cpp:468
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Активировать панель терминала"

#: kateconsole.cpp:538
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"&Синхронизировать терминал с текущим документом автоматически (если возможно)"

#: kateconsole.cpp:542 kateconsole.cpp:563
#, kde-format
msgid "Run in terminal"
msgstr "Запустить в терминале"

#: kateconsole.cpp:544
#, kde-format
msgid "&Remove extension"
msgstr "Удалить рас&ширение"

#: kateconsole.cpp:549
#, kde-format
msgid "Prefix:"
msgstr "Префикс:"

#: kateconsole.cpp:557
#, kde-format
msgid "&Show warning next time"
msgstr "&Показать предупреждение в следующий раз"

#: kateconsole.cpp:559
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"При последующем запуске «%1» показать окно предупреждения, содержащее список "
"команд, предполагаемых к запуску в терминале."

#: kateconsole.cpp:570
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Ус&тановить переменную среды EDITOR равной «kate -b»"

#: kateconsole.cpp:573
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Внимание: чтобы продолжить выполнение консольного приложения, необходимо "
"закрыть этот документ"

#: kateconsole.cpp:576
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Скрывать Konsole по нажатию клавиши «Esc»"

#: kateconsole.cpp:579
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Может вызвать неудобства с консольными программами, обрабатывающими нажатие "
"клавиши «Esc», например vim. Введите названия этих программ в поле, "
"расположенном ниже, разделяя из запятой."

#: kateconsole.cpp:610
#, kde-format
msgid "Terminal Settings"
msgstr "Параметры терминала"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "С&ервис"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Николай Шафоростов"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "shaforostoff@kde.ru"

#~ msgid "Kate Terminal"
#~ msgstr "Терминал для Kate"

#~ msgid "Terminal Panel"
#~ msgstr "Панель терминала"

#~ msgid "Konsole"
#~ msgstr "Терминал"

#~ msgid "Embedded Konsole"
#~ msgstr "Встроенный эмулятор терминала"
